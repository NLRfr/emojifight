# EmojiFight
> Coded so fast I almost didn't do any tests

## Deploy
- `git clone` the repo
- `composer install`
- import the `schema.sql` in MariaDB
- copy the `.env.example` to `.env` and fill the details
- make sure `.env` is blocked on your webserver
- make sure the webserver can write in `emotes/` and `tmp/`
- you're done

## License
MIT
