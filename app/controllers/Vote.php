<?php
class Vote {
  public function beforeRoute($f3) {
    $db = $f3->get('db');
    $f3->set('votes', new DB\SQL\Mapper($db, 'votes'));
  }

  public function submit($f3) {
    $res = 'error';

    try {
      $body = json_decode($f3->get('BODY'));
      $user = $f3->get('SESSION.user')->id;

      $v = $f3->get('votes');
      $v->load(
        array('emote = ? AND user = ?', $body->emote, $user)
      );

      if ($v->dry()) $v->reset();
      $v->emote = $body->emote;
      $v->user = $user;
      $v->vote = $body->vote;

      $v->save();
      $res = 'ok';
    } catch (Exception $e) {
      $res = $e->getMessage();
    }

    header('Content-Type: application/json');
    echo json_encode(array(
      'result' => $res
    ));
  }
}
