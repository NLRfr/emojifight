<?php
class Submit {
  public function beforeRoute($f3) {
    $db = $f3->get('db');
    $f3->set('emotes', new DB\SQL\Mapper($db, 'emotes'));
  }

  public function upload($f3) {
    global $name;
    $name = $f3->get('POST.name');
    $name = \Web::instance()->slug(substr($name, 0, 32));

    $e = $f3->get('emotes');
    $e->load(
      array('name = ?', $name)
    );

    if (!$f3->get('SESSION.user')) {
      $f3->set('SESSION.error', '<strong>L\'envoi a échoué.</strong><br>Session expirée, veuillez vous reconnecter !');
    } elseif (!$name || empty($name)){
      $f3->set('SESSION.error', '<strong>L\'envoi a échoué.</strong><br>Pas de nom pour l\'emote ?');
    } elseif (!$e->dry()) {
      $f3->set('SESSION.error', '<strong>L\'envoi a échoué.</strong><br>Ce nom est déjà pris.');
    } else {
      $e->reset();
      $e->name = $name;
      $e->submitterName = $f3->get('SESSION.user')->username;
      $e->submitterID = $f3->get('SESSION.user')->id;

      $i = 0;
      $web = \Web::instance();
      $files = $web->receive(
        function ($file, $formFieldName) {
          $i++;
          if ($i > 1) return false;

          if ($formFieldName != 'emote' ||
            $file['size'] > (256 * 1000) ||
            $file['type'] != 'image/png')
            return false;

          return true;
        },
        false, // no overwrite
        function ($fileName, $formFieldName) {
          global $name;
          return $name . '.png';
        }
      );

      foreach ($files as $file) {
        if ($file) {
          $e->save();
          $f3->set('SESSION.success', '<strong>Voilà !</strong><br><code>' . $web->slug($name) . '</code> est envoyé !');
        } else {
          $f3->set('SESSION.error', '<strong>L\'envoi a échoué.</strong><br>Est-ce bien un PNG, ne dépasse-t-il pas les 256kb et ce nom n\'est-il pas déjà pris ?');
        }
      }
    }

    $f3->reroute('/');
  }
}
